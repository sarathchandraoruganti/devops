package com.eidiko.devops;

public class Credentials {
    public static String URL = System.getenv("MYSQLDB_URL") ;
    public static String user=System.getenv("MySQLDB_USER");
    public static String password=System.getenv("MYSQLDB_PASSWORD");
    public static String port=System.getenv("MYSQLDB_PORT");
    public static String database = "eidiko";
    public static String CreditCardTable="credit_card_table";
    public static String CustomerDetails="customer_details";
}
