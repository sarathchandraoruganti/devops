FROM  sarathchandra24/centos-tomcat933:latest

RUN rm -rf /opt/tomcat/webapps/*

COPY ./lib/* /usr/bin/
COPY ./lib/* /usr/local/tomcat/lib/

RUN mkdir /usr/temp
COPY ./src/main/java/com/eidiko/devops/dbs/* /usr/temp/

COPY ./target/devops-0.0.1-SNAPSHOT.war /opt/tomcat/webapps/test.war

EXPOSE 8080

CMD ["/opt/tomcat/bin/catalina.sh", "run"]

#docker build -t sarathchandra24/devops .